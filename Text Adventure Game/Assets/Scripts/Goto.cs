﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/InputActions/Goto")]
public class Goto : InputAction
{
    public override void respond_to_input(GameController controller, string[] seperated_input_words)
    {
        string[] input_words_final = new string[2];


        if (seperated_input_words.Length == 1)
        {
            input_words_final[0] = seperated_input_words[0];
            input_words_final[1] = " ";
            controller.roomNavigation.attempt_to_change_rooms(input_words_final[1]);
        }
        else
        {
            controller.roomNavigation.attempt_to_change_rooms(seperated_input_words[1]);
        }
        
    }

}
