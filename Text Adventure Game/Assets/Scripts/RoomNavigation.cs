﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomNavigation : MonoBehaviour {

    public Room currentRoom;

    Dictionary<string, Room> exitDictionary = new Dictionary<string, Room>();


    GameController controller;

    void Awake()
    {
        controller = GetComponent<GameController>();
    }

    public void unpack_exits_in_room()
    {
        for (int i = 0; i < currentRoom.exits.Length; i++)
        {
            //for (int j = 0; j < currentRoom.exits[i].keyStrings.Length; j++)
            //{
                exitDictionary.Add(currentRoom.exits[i].main_string, currentRoom.exits[i].value_room);
                controller.interaction_descriptions_in_room.Add(currentRoom.exits[i].exit_description);
            //}
            

        }

    }

    public void attempt_to_change_rooms(string direction_noun)
    {
        string direction_noun_text = direction_noun;

        foreach (string key in exitDictionary.Keys)
        {
            string[] split_strings = key.Split(',');
            if (split_strings.Contains(direction_noun))
            {
                direction_noun = key;
                direction_noun_text = split_strings[0];
            }

        }


        if (exitDictionary.ContainsKey(direction_noun))
        {
            currentRoom = exitDictionary[direction_noun];
            controller.LogStringWithReturn("You head off to the " + direction_noun_text);
            controller.display_room_text();
        }
        else if ((direction_noun == "") || (direction_noun == " "))
        {
            controller.LogStringWithReturn("Goto, where?");
        }
        else
        {
            controller.LogStringWithReturn("There is no path to the '" + direction_noun + "'");
        }

    }

    public void clear_exits()
    {
        exitDictionary.Clear();
    }

}
