﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TextAdventure/Item")]
public class Item : ScriptableObject
{
    [TextArea]
    public string pickup_description;
    [TextArea]
    public string room_description;
    public string[] item_names;

}
