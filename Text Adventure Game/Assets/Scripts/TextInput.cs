﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextInput : MonoBehaviour {

    public InputField input_field;

    GameController controller;

    void Awake()
    {
        controller = GetComponent<GameController>();


        input_field.onEndEdit.AddListener(accept_string_input);
        

    }

	void accept_string_input(string user_input)
    {
        user_input = user_input.ToLower();  

        controller.LogStringWithReturn(user_input);

        char[] delimiterCharacters = { ' ' };
        string[] seperated_input_words = user_input.Split(delimiterCharacters);

        for (int i = 0; i < controller.inputActions.Length; i++)
        {
            InputAction input_action = controller.inputActions[i];
            if(input_action.keyWord == seperated_input_words[0])
            {
                input_action.respond_to_input(controller, seperated_input_words);
            }
            else
            {
                unknown_input(seperated_input_words);
            }
           
        }

        input_complete();

    }

    void unknown_input(string[] input_words)
    {
        string input_phrase = input_words[0];

        controller.LogStringWithReturn("Command: '"+ input_phrase +"' not known.");
    }

    void input_complete()
    {
        controller.displayLoggedText();
        input_field.ActivateInputField();
        input_field.text = null;

    }
}
