﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public Text displayText;
    public InputAction[] inputActions;

    
    

    [HideInInspector] public RoomNavigation roomNavigation;
    [HideInInspector] public List<string> interaction_descriptions_in_room = new List<string>();


    List<string> action_log = new List<string>();

	// Use this for initialization
	void Awake () {
        roomNavigation = GetComponent<RoomNavigation>();
        
    
	}

    void Start()
    {
        display_room_text();
        displayLoggedText();
        

    }
	
    public void displayLoggedText()
    {
        string logAsText = string.Join("\n", action_log.ToArray());

        displayText.text = logAsText;
    }

    public void display_room_text()
    {
        clear_collections_for_new_room();

        unpackRoom();

        string joined_interaction_descriptions = string.Join("\n", interaction_descriptions_in_room.ToArray());
        
        string combined_text = roomNavigation.currentRoom.description + "\n" + joined_interaction_descriptions;

        LogStringWithReturn(combined_text);
    }

    private void unpackRoom()
    {
        roomNavigation.unpack_exits_in_room();
    }

    void clear_collections_for_new_room()
    {
        interaction_descriptions_in_room.Clear();
        roomNavigation.clear_exits();
    }

    public void LogStringWithReturn(string stringToAdd)
    {
        action_log.Add(stringToAdd + "\n");
    }

	// Update is called once per frame
	void Update () {
		
	}
}
