﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Exit
{
    public string main_string;
    public string exit_description;
    public Room value_room;

}
